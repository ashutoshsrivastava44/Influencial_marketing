var fb= require(‘./controller/fb’);
// API Server Endpoints
module.exports = function(router){
 router.post(‘/fb’, fb.create),
 router.get(‘/fb/:id’, fb.get),
 router.put(‘/fb/:id’, fb.update),
 router.delete(‘/fb/:id’, fb.delete)
}