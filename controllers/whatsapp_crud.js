'use strict';

var whatsapp = require('../model/whatsapp').whatsapp;


exports.create = function (req, res) {
    whatsapp.create(req.body, function(err, result) {
        if (!err) {
            return res.json(result);
        } else {
            return res.send(err); 
        }
    });
};

/** getwhatsapp function to get whatsapp by id. */
exports.get = function (req, res) {
    whatsapp.get({_id: req.params.id}, function(err, result) {
        if (!err) {
            return res.json(result);
        } else {
            return res.send(err); 
        }
    });
};

/** updatewhatsapp function to get whatsapp by id. */
exports.update = function (req, res) {
    whatsapp.updateById(req.params.id, req.body, function(err, result) {
        if (!err) {
            return res.json(result);
        } else {
            return res.send(err); 
        }
    });
}

/** removewhatsapp function to get whatsapp by id. */
exports.delete = function (req, res) {
    whatsapp.removeById({_id: req.params.id}, function(err, result) {
        if (!err) {
            return res.json(result);
        } else {
            console.log(err);
            return res.send(err); 
        }
    });
}