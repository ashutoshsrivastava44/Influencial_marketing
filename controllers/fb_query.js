 module.exports = {
     fb_query = function () {

         var influencialsfb = require('./model/fb.js');
         var type = req.body.type;
         var area = req.body.area;


         // Opens a generic Mongoose Query. Depending on the post body we will...
         var query = fb.find({});

         // ...include filter by Max Distance (converting miles to meters)
         if (type) {
             query = query.where('type').equals(type);
         }
         if (area) {
             query = query.where('area').equals(area);
         }
         // ... Other queries will go here ... 

         // Execute Query and Return the Query Results
         query.exec(function (err, influentials) {
             if (err)
                 res.send(err);

             // If no errors, respond with a JSON of all users that meet the criteria
             res.json(influencials);
         });
     }
 };