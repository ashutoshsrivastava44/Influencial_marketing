'use strict';

var fb = require('../model/fb').fb;


exports.create = function (req, res) {
    fb.create(req.body, function(err, result) {
        if (!err) {
            return res.json(result);
        } else {
            return res.send(err); 
        }
    });
};

/** getfb function to get fb by id. */
exports.get = function (req, res) {
    fb.get({_id: req.params.id}, function(err, result) {
        if (!err) {
            return res.json(result);
        } else {
            return res.send(err); 
        }
    });
};

/** updatefb function to get fb by id. */
exports.update = function (req, res) {
    fb.updateById(req.params.id, req.body, function(err, result) {
        if (!err) {
            return res.json(result);
        } else {
            return res.send(err); 
        }
    });
}

/** removefb function to get fb by id. */
exports.delete = function (req, res) {
    fb.removeById({_id: req.params.id}, function(err, result) {
        if (!err) {
            return res.json(result);
        } else {
            console.log(err);
            return res.send(err); 
        }
    });
}