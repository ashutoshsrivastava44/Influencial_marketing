const bcrypt = require('bcrypt-nodejs');
const crypto = require('crypto');
const mongoose = require('mongoose');


//create schema for blog post
var WhatsAppSchema = new mongoose.Schema({
  id: Number;
  admin: Array,
  members: Number,
  type: String,
  area: String,
  
});

WhatsAppSchema.statics = {

    
    get: function(query, callback) {
        this.findOne(query, callback);
    },
   
    getAll: function(query, callback) {
        this.find(query, callback);
    },
    
   
    updateById: function(id, updateData, callback) {
        this.update(id, {$set: updateData}, callback);
    },
    remove: function(removeData, callback) {
         this.remove(removeData, callback);
    },
    create: function(data, callback) {
        var WhatsApp = new this(data);
        WhatsApp.save(callback);
    }
}

const WhatsApp = mongoose.model('WhatsApp', WhatsAppSchema);
module.exports = User;