const bcrypt = require('bcrypt-nodejs');
const crypto = require('crypto');
const mongoose = require('mongoose');


//create schema for blog post
var fbSchema = new mongoose.Schema({
	
  id: Number;	
  likes:  Number,
  owner: String,
  type:   String,
  area: String,
  
});

fbSchema.statics = {

     
    get: function(query, callback) {
        this.findOne(query, callback);
    },
   
    getAll: function(query, callback) {
        this.find(query, callback);
    },
    
   
    updateById: function(id, updateData, callback) {
        this.update(id, {$set: updateData}, callback);
    },
    remove: function(removeData, callback) {
         this.remove(removeData, callback);
    },
    create: function(data, callback) {
        var fb = new this(data);
        fb.save(callback);
    }
}

const fb = mongoose.model('fb', fbSchema);
module.exports = fb;